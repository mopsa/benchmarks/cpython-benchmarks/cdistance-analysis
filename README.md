This repository is a copy of the [cdistance library](https://github.com/doukremt/distance), using commit ad7f9dc7e9b0e88a08d0cefd1442f4ab1dd1779b.

Run `make` to build the library using `mopsa-build` and launch the analysis of the tests.
The results are written in text files by default.

Minor changes performed to analyze the library are shown in CHANGELOG.md
