from ._fastcomp import fast_comp
from ._lcsubstrings import lcsubstrings
from ._levenshtein import levenshtein, nlevenshtein
from ._simpledists import hamming, jaccard, sorensen,
# from ._iterators import ilevenshtein, ifast_comp
