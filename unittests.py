import unittest

from distance import fast_comp, hamming, ifast_comp, ilevenshtein, lcsubstrings, levenshtein, nlevenshtein
c = 0

# we could create a class Tests: ... with the tests, then make
# UnicodeTests etc inherit from Tests and unittest.TestCase and
# defining t. However, assertions are grouped by range and this would
# be less interesting...

class UnicodeTests(unittest.TestCase):
    def t(self, s): return s

    def test_hamming(self):
        func = hamming

        with self.assertRaises(ValueError):
            func(1, self.t("foo"))
        with self.assertRaises(ValueError):
            func(self.t("foo"), 1)

        self.assertEqual(func(self.t(""), self.t("")), 0)
        self.assertEqual(func(self.t("abc"), self.t("abc")), 0)
        self.assertEqual(func(self.t("abc"), self.t("abd")), 1)

        with self.assertRaises(ValueError):
            func(self.t("foo"), self.t("foobar"))

        with self.assertRaises(ValueError):
            func(self.t(""), self.t("foo"))

        self.assertEqual(func(self.t(""), self.t(""), normalized=True), 0)
        self.assertEqual(func(self.t("abc"), self.t("abc"), normalized=True), 0)
        self.assertEqual(func(self.t("ab"), self.t("ac"), normalized=True), 0.5)
        self.assertEqual(func(self.t("abc"), self.t("def"), normalized=True), 1.0)


    def test_fast_comp(self):
        func = fast_comp

        with self.assertRaises(ValueError):
            func(1, self.t("foo"))
        with self.assertRaises(ValueError):
            func(self.t("foo"), 1)

        self.assertEqual(func(self.t(""), self.t("")), 0)
        self.assertEqual(func(self.t(""), self.t("a")) == func(self.t("a"), self.t("")), 1)

        self.assertEqual(func(self.t("aa"), self.t("aa")), 0)
        self.assertEqual(func(self.t("ab"), self.t("aa")), 1)
        self.assertEqual(func(self.t("ab"), self.t("a")), 1)
        self.assertEqual(func(self.t("ab"), self.t("abc")), 1)

        self.assertEqual(func(self.t("a"), self.t("bcd")), -1)
        self.assertEqual(func(self.t("bcd"), self.t("a")), -1)

        self.assertEqual(func(self.t("abc"), self.t("bac"), transpositions=True), 1)
        self.assertEqual(func(self.t("bac"), self.t("abc"), transpositions=True), 1)

    def test_levenshtein(self):
        func = levenshtein

        with self.assertRaises(ValueError):
            func(1, self.t("foo"))
        with self.assertRaises(ValueError):
            func(self.t("foo"), 1)

        self.assertEqual(func(self.t(""), self.t("")), 0)
        self.assertEqual(func(self.t(""), self.t("abcd")), 4)
        self.assertEqual(func(self.t("abcd"), self.t("")), 4)

        self.assertEqual(func(self.t("aa"), self.t("aa")), 0)
        self.assertEqual(func(self.t("ab"), self.t("aa")), 1)
        self.assertEqual(func(self.t("ab"), self.t("a")), 1)
        self.assertEqual(func(self.t("ab"), self.t("abc")), 1)

        self.assertEqual(func(self.t("a"), self.t("b"), max_dist=0), -1)
        self.assertEqual(func(self.t("a"), self.t("b"), max_dist=1), 1)
        self.assertEqual(func(self.t("foo"), self.t("bar"), max_dist=-1), 3)

    def test_nlevenshtein(self):
        func = nlevenshtein

        with self.assertRaises(ValueError):
            func(1, self.t("foo"))
        with self.assertRaises(ValueError):
            func(self.t("foo"), 1)

        self.assertEqual(func(self.t(""), self.t(""), 1), 0)
        self.assertEqual(func(self.t(""), self.t(""), 2), 0)
        self.assertEqual(func(self.t(""), self.t("foo"), 1), 1)
        self.assertEqual(func(self.t("foo"), self.t(""), 1), 1)
        self.assertEqual(func(self.t(""), self.t("foo"), 2), 1)
        self.assertEqual(func(self.t("foo"), self.t(""), 2), 1)

        self.assertEqual(func(self.t("aa"), self.t("aa"), 1), 0)
        self.assertEqual(func(self.t("aa"), self.t("aa"), 2), 0)
        self.assertEqual(func(self.t("ab"), self.t("aa"), 1), 0.5)
        self.assertEqual(func(self.t("ab"), self.t("aa"), 2), 0.5)
        self.assertEqual(func(self.t("ab"), self.t("a"), 1), 0.5)
        self.assertEqual(func(self.t("ab"), self.t("a"), 2), 0.5)
        self.assertEqual(func(self.t("ab"), self.t("abc"), 1), 0.3333333333333333)
        self.assertEqual(func(self.t("ab"), self.t("abc"), 2), 0.3333333333333333)

        self.assertEqual(func(self.t("abc"), self.t("adb"), 1), 0.6666666666666666)
        self.assertEqual(func(self.t("abc"), self.t("adb"), 2), 0.5)

    def test_lcsubstrings(self):
        func = lcsubstrings

        with self.assertRaises(ValueError):
            func(1, self.t("foo"))
        with self.assertRaises(ValueError):
            func(self.t("foo"), 1)

        self.assertEqual(func(self.t(""), self.t(""), False), set())
       	self.assertEqual(func(self.t(""), self.t("foo"), False), set())
        self.assertEqual(func(self.t("foo"), self.t(""), False), set())
       	self.assertEqual(func(self.t("abcd"), self.t("cdba"), False), {self.t('cd')})
       	self.assertEqual(func(self.t("abcdef"), self.t("cdba"), False), func(self.t("cdba"), self.t("abcdef"), False))

        self.assertEqual(func(self.t(""), self.t(""), True), (0, ()))
        self.assertEqual(func(self.t(""), self.t("foo"), True), (0, ()))
        self.assertEqual(func(self.t("foo"), self.t(""), True), (0, ()))

        self.assertEqual(func(self.t("abcd"), self.t("cdba"), True), (2, ((2, 0),)))
        self.assertEqual(func(self.t("abcdef"), self.t("cdba"), True), func(self.t("cdba"), self.t("abcdef"), True))

    def test_ilevenshtein(self):
        func = ilevenshtein

        with self.assertRaises(ValueError):
            func(1, self.t("foo"), max_dist=2)

        itor = func(self.t("foo"), [self.t("foo"), 3333], max_dist=2)
       	self.assertEqual(next(itor), (0, self.t('foo')))
       	with self.assertRaises(ValueError):
            next(itor)

        itor = func(self.t("aa"), [self.t("aa"), self.t("abcd"), self.t("ba")], max_dist=2)
        self.assertEqual(next(itor), (0, self.t("aa")))
        self.assertEqual(next(itor), (1, self.t("ba")))

    def test_ifast_comp(self):
        func = ifast_comp

        with self.assertRaises(ValueError):
            func(1, self.t("foo"))

        itor = func(self.t("foo"), [self.t("foo"), 3333])
       	self.assertEqual(next(itor), (0, self.t('foo')))
       	with self.assertRaises(ValueError):
            next(itor)

        itor = func(self.t("aa"), [self.t("aa"), self.t("abcd"), self.t("ba")])
        self.assertEqual(next(itor), (0, self.t("aa")))
        self.assertEqual(next(itor), (1, self.t("ba")))

        g = func(self.t("abc"), [self.t("bac")], transpositions=False)
        self.assertEqual(next(g), (2, self.t('bac')))
        g = func(self.t("abc"), [self.t("bac")], transpositions=True)
        self.assertEqual(next(g), (1, self.t("bac")))


class BytesTests(unittest.TestCase):
    def t(self, s): return s.encode()

    def test_hamming(self):
        func = hamming

        with self.assertRaises(ValueError):
            func(1, self.t("foo"))
        with self.assertRaises(ValueError):
            func(self.t("foo"), 1)

        self.assertEqual(func(self.t(""), self.t("")), 0)
        self.assertEqual(func(self.t("abc"), self.t("abc")), 0)
        self.assertEqual(func(self.t("abc"), self.t("abd")), 1)

        with self.assertRaises(ValueError):
            func(self.t("foo"), self.t("foobar"))

        with self.assertRaises(ValueError):
            func(self.t(""), self.t("foo"))

        self.assertEqual(func(self.t(""), self.t(""), normalized=True), 0)
        self.assertEqual(func(self.t("abc"), self.t("abc"), normalized=True), 0)
        self.assertEqual(func(self.t("ab"), self.t("ac"), normalized=True), 0.5)
        self.assertEqual(func(self.t("abc"), self.t("def"), normalized=True), 1.0)


    def test_fast_comp(self):
        func = fast_comp

        with self.assertRaises(ValueError):
            func(1, self.t("foo"))
        with self.assertRaises(ValueError):
            func(self.t("foo"), 1)

        self.assertEqual(func(self.t(""), self.t("")), 0)
        self.assertEqual(func(self.t(""), self.t("a")) == func(self.t("a"), self.t("")), 1)

        self.assertEqual(func(self.t("aa"), self.t("aa")), 0)
        self.assertEqual(func(self.t("ab"), self.t("aa")), 1)
        self.assertEqual(func(self.t("ab"), self.t("a")), 1)
        self.assertEqual(func(self.t("ab"), self.t("abc")), 1)

        self.assertEqual(func(self.t("a"), self.t("bcd")), -1)
        self.assertEqual(func(self.t("bcd"), self.t("a")), -1)

        self.assertEqual(func(self.t("abc"), self.t("bac"), transpositions=True), 1)
        self.assertEqual(func(self.t("bac"), self.t("abc"), transpositions=True), 1)

    def test_levenshtein(self):
        func = levenshtein

        with self.assertRaises(ValueError):
            func(1, self.t("foo"))
        with self.assertRaises(ValueError):
            func(self.t("foo"), 1)

        self.assertEqual(func(self.t(""), self.t("")), 0)
        self.assertEqual(func(self.t(""), self.t("abcd")), 4)
        self.assertEqual(func(self.t("abcd"), self.t("")), 4)

        self.assertEqual(func(self.t("aa"), self.t("aa")), 0)
        self.assertEqual(func(self.t("ab"), self.t("aa")), 1)
        self.assertEqual(func(self.t("ab"), self.t("a")), 1)
        self.assertEqual(func(self.t("ab"), self.t("abc")), 1)

        self.assertEqual(func(self.t("a"), self.t("b"), max_dist=0), -1)
        self.assertEqual(func(self.t("a"), self.t("b"), max_dist=1), 1)
        self.assertEqual(func(self.t("foo"), self.t("bar"), max_dist=-1), 3)

    def test_nlevenshtein(self):
        func = nlevenshtein

        with self.assertRaises(ValueError):
            func(1, self.t("foo"))
        with self.assertRaises(ValueError):
            func(self.t("foo"), 1)

        self.assertEqual(func(self.t(""), self.t(""), 1), 0)
        self.assertEqual(func(self.t(""), self.t(""), 2), 0)
        self.assertEqual(func(self.t(""), self.t("foo"), 1), 1)
        self.assertEqual(func(self.t("foo"), self.t(""), 1), 1)
        self.assertEqual(func(self.t(""), self.t("foo"), 2), 1)
        self.assertEqual(func(self.t("foo"), self.t(""), 2), 1)

        self.assertEqual(func(self.t("aa"), self.t("aa"), 1), 0)
        self.assertEqual(func(self.t("aa"), self.t("aa"), 2), 0)
        self.assertEqual(func(self.t("ab"), self.t("aa"), 1), 0.5)
        self.assertEqual(func(self.t("ab"), self.t("aa"), 2), 0.5)
        self.assertEqual(func(self.t("ab"), self.t("a"), 1), 0.5)
        self.assertEqual(func(self.t("ab"), self.t("a"), 2), 0.5)
        self.assertEqual(func(self.t("ab"), self.t("abc"), 1), 0.3333333333333333)
        self.assertEqual(func(self.t("ab"), self.t("abc"), 2), 0.3333333333333333)

        self.assertEqual(func(self.t("abc"), self.t("adb"), 1), 0.6666666666666666)
        self.assertEqual(func(self.t("abc"), self.t("adb"), 2), 0.5)

    def test_lcsubstrings(self):
        func = lcsubstrings

        with self.assertRaises(ValueError):
            func(1, self.t("foo"))
        with self.assertRaises(ValueError):
            func(self.t("foo"), 1)

        self.assertEqual(func(self.t(""), self.t(""), False), set())
       	self.assertEqual(func(self.t(""), self.t("foo"), False), set())
        self.assertEqual(func(self.t("foo"), self.t(""), False), set())
       	self.assertEqual(func(self.t("abcd"), self.t("cdba"), False), {self.t('cd')})
       	self.assertEqual(func(self.t("abcdef"), self.t("cdba"), False), func(self.t("cdba"), self.t("abcdef"), False))

        # self.assertEqual(func(self.t(""), self.t(""), True), (0, ()))
        # self.assertEqual(func(self.t(""), self.t("foo"), True), (0, ()))
        # self.assertEqual(func(self.t("foo"), self.t(""), True), (0, ()))

        # self.assertEqual(func(self.t("abcd"), self.t("cdba"), True), (2, ((2, 0),)))
        # self.assertEqual(func(self.t("abcdef"), self.t("cdba"), True), func(self.t("cdba"), self.t("abcdef"), True))

    def test_ilevenshtein(self):
        func = ilevenshtein

        with self.assertRaises(ValueError):
            func(1, self.t("foo"), max_dist=2)

        itor = func(self.t("foo"), [self.t("foo"), 3333], max_dist=2)
       	self.assertEqual(next(itor), (0, self.t('foo')))
       	with self.assertRaises(ValueError):
            next(itor)

        itor = func(self.t("aa"), [self.t("aa"), self.t("abcd"), self.t("ba")], max_dist=2)
        self.assertEqual(next(itor), (0, self.t("aa")))
        self.assertEqual(next(itor), (1, self.t("ba")))

    def test_ifast_comp(self):
        func = ifast_comp

        with self.assertRaises(ValueError):
            func(1, self.t("foo"))

        itor = func(self.t("foo"), [self.t("foo"), 3333])
       	self.assertEqual(next(itor), (0, self.t('foo')))
       	with self.assertRaises(ValueError):
            next(itor)

        itor = func(self.t("aa"), [self.t("aa"), self.t("abcd"), self.t("ba")])
        self.assertEqual(next(itor), (0, self.t("aa")))
        self.assertEqual(next(itor), (1, self.t("ba")))

        g = func(self.t("abc"), [self.t("bac")], transpositions=False)
        self.assertEqual(next(g), (2, self.t('bac')))
        g = func(self.t("abc"), [self.t("bac")], transpositions=True)
        self.assertEqual(next(g), (1, self.t("bac")))

class ListTests(unittest.TestCase):
    def t(self, s): return list(s)

    def test_hamming(self):
        func = hamming

        with self.assertRaises(ValueError):
            func(1, self.t("foo"))
        with self.assertRaises(ValueError):
            func(self.t("foo"), 1)

        self.assertEqual(func(self.t(""), self.t("")), 0)
        self.assertEqual(func(self.t("abc"), self.t("abc")), 0)
        self.assertEqual(func(self.t("abc"), self.t("abd")), 1)

        with self.assertRaises(ValueError):
            func(self.t("foo"), self.t("foobar"))

        with self.assertRaises(ValueError):
            func(self.t(""), self.t("foo"))

        self.assertEqual(func(self.t(""), self.t(""), normalized=True), 0)
        self.assertEqual(func(self.t("abc"), self.t("abc"), normalized=True), 0)
        self.assertEqual(func(self.t("ab"), self.t("ac"), normalized=True), 0.5)
        self.assertEqual(func(self.t("abc"), self.t("def"), normalized=True), 1.0)


    def test_fast_comp(self):
        func = fast_comp

        with self.assertRaises(ValueError):
            func(1, self.t("foo"))
        with self.assertRaises(ValueError):
            func(self.t("foo"), 1)

        self.assertEqual(func(self.t(""), self.t("")), 0)
        self.assertEqual(func(self.t(""), self.t("a")) == func(self.t("a"), self.t("")), 1)

        self.assertEqual(func(self.t("aa"), self.t("aa")), 0)
        self.assertEqual(func(self.t("ab"), self.t("aa")), 1)
        self.assertEqual(func(self.t("ab"), self.t("a")), 1)
        self.assertEqual(func(self.t("ab"), self.t("abc")), 1)

        self.assertEqual(func(self.t("a"), self.t("bcd")), -1)
        self.assertEqual(func(self.t("bcd"), self.t("a")), -1)

        self.assertEqual(func(self.t("abc"), self.t("bac"), transpositions=True), 1)
        self.assertEqual(func(self.t("bac"), self.t("abc"), transpositions=True), 1)

    def test_levenshtein(self):
        func = levenshtein

        with self.assertRaises(ValueError):
            func(1, self.t("foo"))
        with self.assertRaises(ValueError):
            func(self.t("foo"), 1)

        self.assertEqual(func(self.t(""), self.t("")), 0)
        self.assertEqual(func(self.t(""), self.t("abcd")), 4)
        self.assertEqual(func(self.t("abcd"), self.t("")), 4)

        self.assertEqual(func(self.t("aa"), self.t("aa")), 0)
        self.assertEqual(func(self.t("ab"), self.t("aa")), 1)
        self.assertEqual(func(self.t("ab"), self.t("a")), 1)
        self.assertEqual(func(self.t("ab"), self.t("abc")), 1)

        self.assertEqual(func(self.t("a"), self.t("b"), max_dist=0), -1)
        self.assertEqual(func(self.t("a"), self.t("b"), max_dist=1), 1)
        self.assertEqual(func(self.t("foo"), self.t("bar"), max_dist=-1), 3)

    def test_nlevenshtein(self):
        func = nlevenshtein

        with self.assertRaises(ValueError):
            func(1, self.t("foo"))
        with self.assertRaises(ValueError):
            func(self.t("foo"), 1)

        self.assertEqual(func(self.t(""), self.t(""), 1), 0)
        self.assertEqual(func(self.t(""), self.t(""), 2), 0)
        self.assertEqual(func(self.t(""), self.t("foo"), 1), 1)
        self.assertEqual(func(self.t("foo"), self.t(""), 1), 1)
        self.assertEqual(func(self.t(""), self.t("foo"), 2), 1)
        self.assertEqual(func(self.t("foo"), self.t(""), 2), 1)

        self.assertEqual(func(self.t("aa"), self.t("aa"), 1), 0)
        self.assertEqual(func(self.t("aa"), self.t("aa"), 2), 0)
        self.assertEqual(func(self.t("ab"), self.t("aa"), 1), 0.5)
        self.assertEqual(func(self.t("ab"), self.t("aa"), 2), 0.5)
        self.assertEqual(func(self.t("ab"), self.t("a"), 1), 0.5)
        self.assertEqual(func(self.t("ab"), self.t("a"), 2), 0.5)
        self.assertEqual(func(self.t("ab"), self.t("abc"), 1), 0.3333333333333333)
        self.assertEqual(func(self.t("ab"), self.t("abc"), 2), 0.3333333333333333)

        self.assertEqual(func(self.t("abc"), self.t("adb"), 1), 0.6666666666666666)
        self.assertEqual(func(self.t("abc"), self.t("adb"), 2), 0.5)

    def test_lcsubstrings(self):
        func = lcsubstrings

        with self.assertRaises(ValueError):
            func(1, self.t("foo"))
        with self.assertRaises(ValueError):
            func(self.t("foo"), 1)

        with self.assertRaises(TypeError):
            func(self.t(""), self.t(""), False)
        with self.assertRaises(TypeError):
       	    func(self.t(""), self.t("foo"), False)
        with self.assertRaises(TypeError):
            func(self.t("foo"), self.t(""), False)
        with self.assertRaises(TypeError):
       	    func(self.t("abcd"), self.t("cdba"), False)
        with self.assertRaises(TypeError):
       	    func(self.t("abcdef"), self.t("cdba"), False)

        # self.assertEqual(func(self.t(""), self.t(""), True), (0, ()))
        # self.assertEqual(func(self.t(""), self.t("foo"), True), (0, ()))
        # self.assertEqual(func(self.t("foo"), self.t(""), True), (0, ()))

        # self.assertEqual(func(self.t("abcd"), self.t("cdba"), True), (2, ((2, 0),)))
        # self.assertEqual(func(self.t("abcdef"), self.t("cdba"), True), func(self.t("cdba"), self.t("abcdef"), True))

    def test_ilevenshtein(self):
        func = ilevenshtein

        with self.assertRaises(ValueError):
            func(1, self.t("foo"), max_dist=2)

        itor = func(self.t("foo"), [self.t("foo"), 3333], max_dist=2)
       	self.assertEqual(next(itor), (0, self.t('foo')))
       	with self.assertRaises(ValueError):
            next(itor)

        itor = func(self.t("aa"), [self.t("aa"), self.t("abcd"), self.t("ba")], max_dist=2)
        self.assertEqual(next(itor), (0, self.t("aa")))
        self.assertEqual(next(itor), (1, self.t("ba")))

    def test_ifast_comp(self):
        func = ifast_comp

        with self.assertRaises(ValueError):
            func(1, self.t("foo"))

        itor = func(self.t("foo"), [self.t("foo"), 3333])
       	self.assertEqual(next(itor), (0, self.t('foo')))
       	with self.assertRaises(ValueError):
            next(itor)

        itor = func(self.t("aa"), [self.t("aa"), self.t("abcd"), self.t("ba")])
        self.assertEqual(next(itor), (0, self.t("aa")))
        self.assertEqual(next(itor), (1, self.t("ba")))

        g = func(self.t("abc"), [self.t("bac")], transpositions=False)
        self.assertEqual(next(g), (2, self.t('bac')))
        g = func(self.t("abc"), [self.t("bac")], transpositions=True)
        self.assertEqual(next(g), (1, self.t("bac")))

class TupleTests(unittest.TestCase):
    def t(self, s): return tuple(s)

    def test_hamming(self):
        func = hamming

        with self.assertRaises(ValueError):
            func(1, self.t("foo"))
        with self.assertRaises(ValueError):
            func(self.t("foo"), 1)

        self.assertEqual(func(self.t(""), self.t("")), 0)
        self.assertEqual(func(self.t("abc"), self.t("abc")), 0)
        self.assertEqual(func(self.t("abc"), self.t("abd")), 1)

        with self.assertRaises(ValueError):
            func(self.t("foo"), self.t("foobar"))

        with self.assertRaises(ValueError):
            func(self.t(""), self.t("foo"))

        self.assertEqual(func(self.t(""), self.t(""), normalized=True), 0)
        self.assertEqual(func(self.t("abc"), self.t("abc"), normalized=True), 0)
        self.assertEqual(func(self.t("ab"), self.t("ac"), normalized=True), 0.5)
        self.assertEqual(func(self.t("abc"), self.t("def"), normalized=True), 1.0)


    def test_fast_comp(self):
        func = fast_comp

        with self.assertRaises(ValueError):
            func(1, self.t("foo"))
        with self.assertRaises(ValueError):
            func(self.t("foo"), 1)

        self.assertEqual(func(self.t(""), self.t("")), 0)
        self.assertEqual(func(self.t(""), self.t("a")) == func(self.t("a"), self.t("")), 1)

        self.assertEqual(func(self.t("aa"), self.t("aa")), 0)
        self.assertEqual(func(self.t("ab"), self.t("aa")), 1)
        self.assertEqual(func(self.t("ab"), self.t("a")), 1)
        self.assertEqual(func(self.t("ab"), self.t("abc")), 1)

        self.assertEqual(func(self.t("a"), self.t("bcd")), -1)
        self.assertEqual(func(self.t("bcd"), self.t("a")), -1)

        self.assertEqual(func(self.t("abc"), self.t("bac"), transpositions=True), 1)
        self.assertEqual(func(self.t("bac"), self.t("abc"), transpositions=True), 1)

    def test_levenshtein(self):
        func = levenshtein

        with self.assertRaises(ValueError):
            func(1, self.t("foo"))
        with self.assertRaises(ValueError):
            func(self.t("foo"), 1)

        self.assertEqual(func(self.t(""), self.t("")), 0)
        self.assertEqual(func(self.t(""), self.t("abcd")), 4)
        self.assertEqual(func(self.t("abcd"), self.t("")), 4)

        self.assertEqual(func(self.t("aa"), self.t("aa")), 0)
        self.assertEqual(func(self.t("ab"), self.t("aa")), 1)
        self.assertEqual(func(self.t("ab"), self.t("a")), 1)
        self.assertEqual(func(self.t("ab"), self.t("abc")), 1)

        self.assertEqual(func(self.t("a"), self.t("b"), max_dist=0), -1)
        self.assertEqual(func(self.t("a"), self.t("b"), max_dist=1), 1)
        self.assertEqual(func(self.t("foo"), self.t("bar"), max_dist=-1), 3)

    def test_nlevenshtein(self):
        func = nlevenshtein

        with self.assertRaises(ValueError):
            func(1, self.t("foo"))
        with self.assertRaises(ValueError):
            func(self.t("foo"), 1)

        self.assertEqual(func(self.t(""), self.t(""), 1), 0)
        self.assertEqual(func(self.t(""), self.t(""), 2), 0)
        self.assertEqual(func(self.t(""), self.t("foo"), 1), 1)
        self.assertEqual(func(self.t("foo"), self.t(""), 1), 1)
        self.assertEqual(func(self.t(""), self.t("foo"), 2), 1)
        self.assertEqual(func(self.t("foo"), self.t(""), 2), 1)

        self.assertEqual(func(self.t("aa"), self.t("aa"), 1), 0)
        self.assertEqual(func(self.t("aa"), self.t("aa"), 2), 0)
        self.assertEqual(func(self.t("ab"), self.t("aa"), 1), 0.5)
        self.assertEqual(func(self.t("ab"), self.t("aa"), 2), 0.5)
        self.assertEqual(func(self.t("ab"), self.t("a"), 1), 0.5)
        self.assertEqual(func(self.t("ab"), self.t("a"), 2), 0.5)
        self.assertEqual(func(self.t("ab"), self.t("abc"), 1), 0.3333333333333333)
        self.assertEqual(func(self.t("ab"), self.t("abc"), 2), 0.3333333333333333)

        self.assertEqual(func(self.t("abc"), self.t("adb"), 1), 0.6666666666666666)
        self.assertEqual(func(self.t("abc"), self.t("adb"), 2), 0.5)

    def test_lcsubstrings(self):
        func = lcsubstrings

        with self.assertRaises(ValueError):
            func(1, self.t("foo"))
        with self.assertRaises(ValueError):
            func(self.t("foo"), 1)

        # FIXME: for lists, the next 5 assertions should raise typeerrors
        self.assertEqual(func(self.t(""), self.t(""), False), set())
       	self.assertEqual(func(self.t(""), self.t("foo"), False), set())
        self.assertEqual(func(self.t("foo"), self.t(""), False), set())
       	self.assertEqual(func(self.t("abcd"), self.t("cdba"), False), {self.t('cd')})
       	self.assertEqual(func(self.t("abcdef"), self.t("cdba"), False), func(self.t("cdba"), self.t("abcdef"), False))

        # self.assertEqual(func(self.t(""), self.t(""), True), (0, ()))
        # self.assertEqual(func(self.t(""), self.t("foo"), True), (0, ()))
        # self.assertEqual(func(self.t("foo"), self.t(""), True), (0, ()))

        # self.assertEqual(func(self.t("abcd"), self.t("cdba"), True), (2, ((2, 0),)))
        # self.assertEqual(func(self.t("abcdef"), self.t("cdba"), True), func(self.t("cdba"), self.t("abcdef"), True))

    def test_ilevenshtein(self):
        func = ilevenshtein

        with self.assertRaises(ValueError):
            func(1, self.t("foo"), max_dist=2)

        itor = func(self.t("foo"), [self.t("foo"), 3333], max_dist=2)
       	self.assertEqual(next(itor), (0, self.t('foo')))
       	with self.assertRaises(ValueError):
            next(itor)

        itor = func(self.t("aa"), [self.t("aa"), self.t("abcd"), self.t("ba")], max_dist=2)
        self.assertEqual(next(itor), (0, self.t("aa")))
        self.assertEqual(next(itor), (1, self.t("ba")))

    def test_ifast_comp(self):
        func = ifast_comp

        with self.assertRaises(ValueError):
            func(1, self.t("foo"))

        itor = func(self.t("foo"), [self.t("foo"), 3333])
       	self.assertEqual(next(itor), (0, self.t('foo')))
       	with self.assertRaises(ValueError):
            next(itor)

        itor = func(self.t("aa"), [self.t("aa"), self.t("abcd"), self.t("ba")])
        self.assertEqual(next(itor), (0, self.t("aa")))
        self.assertEqual(next(itor), (1, self.t("ba")))

        g = func(self.t("abc"), [self.t("bac")], transpositions=False)
        self.assertEqual(next(g), (2, self.t('bac')))
        g = func(self.t("abc"), [self.t("bac")], transpositions=True)
        self.assertEqual(next(g), (1, self.t("bac")))

if __name__ == "__main__":
    unittest.main()
